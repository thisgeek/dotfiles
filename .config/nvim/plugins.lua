--
-- LSP
--

-- TODO
-- - Check classpath handling. Ignore directories?
-- - Needs `lein monolith with-all classpath`?
-- - Can haz jar and zipfile schemas?

-- From https://github.com/neovim/nvim-lspconfig#suggested-configuration

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, bufopts)
end

require"lspconfig".clojure_lsp.setup{
  on_attach = on_attach,
}

-- Disabled due to error
-- module 'cmp' not found
-- require"kanban".setup{
--   markdown = {
--     description_folder = "./tasks/",
--     list_head = "## ",
-- 	}
-- }

-- How to disable the LSP error messages, e.g. if the messages are too many

-- Hint: Use `:lua ` to run from the vim command line
-- https://github.com/neovim/nvim-lspconfig/issues/662#issuecomment-1168067385


--
-- Presenting
--

require "presenting".setup {
  options = { width = 80 },
  separator = {
    markdown = "^-------------------------------------------------------------------------------"
  }
}

--
-- Zen Mode
--

require"zen-mode".setup {
  plugins = {
    kitty = {
      enabled = true,
      font = "+2", -- font size increment
    }
  }
  -- TODO: Omit g prefix for motions in zen mode
  -- If remapping, consider exception like for vcount
  --
  -- Ref: https://stackoverflow.com/questions/20975928/moving-the-cursor-through-long-soft-wrapped-lines-in-vim
  -- ```vim
  -- nnoremap <expr> k (v:count == 0 ? 'gk' : 'k')
  -- nnoremap <expr> j (v:count == 0 ? 'gj' : 'j')
  -- ```
  -- Note: <expr> is a special map argument for `:map-expression`. It makes the argument an expression.
  --
  -- Leverage on_open and on_close functions for remappings
  --on_open = function(win)
  --  -- TODO: remap motion keys to g prefixed equivalents
  --  --
  --  -- WIP lua port of suggested mapping from SO
  --  -- ```lua
  --  -- vim.api.nvim_buf_set_keymap('n', 'j', 'gj', { expr = false, noremap = true })
  --  -- ```
  --  --
  --  -- Notes:
  --  -- * Only sets mapping for the current buffer
  --  -- * Does not yet avoid clobbering the vcount
  --  -- * Does not yet have an on-close counterpart
  --  -- * Does not yet wright the RHS as an expression, hence `expr = false`
  --end,
  --on_close = function()
  --  -- TODO: map motion keys back to normal
  --  -- Note: accedotal experiments suggest that mapping directly back, e.g.
  --  -- `noremap j j`, produces the dedired restoration effect.
  --end,
}

--
-- Tangerine
--

require "tangerine".setup {}
