set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc

" Briefly highlight on yank
au TextYankPost * lua vim.highlight.on_yank {higroup="IncSearch", timeout=300, on_visual=true}

""
"" Searching
""

" Support live substitution
" From https://blog.kdheepak.com/three-builtin-neovim-features#live-substitution
set inccommand=split

""
"" Zen Mode
""

" See also plugins.lua
nmap <leader>vz :ZenMode<CR>
command! Goyo :echoerr "Goyo has been replaced with ZenMode"

""
"" Terminal
""

" Shortcuts to opening a new terminal buffer with split or tabs
nmap <leader>ts :split \| terminal<CR>
nmap <leader>tv :vsplit \| terminal <CR>
nmap <silent><leader>tt :tabnew \| terminal<CR>

nmap <silent><leader>tc :Console<CR>
" g for Go as an alias to Start
nmap <silent><leader>tg :Start<CR>

" Locally omit line numbers opening a terminal
" Form https://stackoverflow.com/a/63908546/199644
" & https://stackoverflow.com/a/63909865/199644
augroup neovim_terminal
    autocmd!
    " Disables number lines on terminal buffers
    autocmd TermOpen * :setlocal nonumber norelativenumber
augroup END

" Allow window nav mappings to auto exit terminal
tmap <C-w> <C-\><C-n><C-w>

" Workaround for NeoVim's esc sequence handling: prevents accidental esc while
" typing shift in terminal
" Based on discussion: https://github.com/vim/vim/issues/6040
tnoremap <S-Space> <Space>


""
"" GH.nvim
""

lua << EOF
require"litee.lib".setup{
  -- this is where you configure details about your panel, such as
  -- whether it toggles on the left, right, top, or bottom.
  -- leaving this blank will use the defaults.
  -- reminder: gh.nvim uses litee.lib to implement core portions of its UI.
}
require"litee.gh".setup{
  -- this is where you configure details about gh.nvim directly relating
  -- to GitHub integration.
}
EOF

""
"" Which Key
""

lua << EOF
require"which-key".setup { }
EOF

""
"" Load Lua
""

luafile $HOME/.config/nvim/plugins.lua

""
"" Healthcheck
""
let g:loaded_python3_provider = 0
let g:loaded_ruby_provider = 0
let g:loaded_perl_provider = 0

""
"" psql.nvim
""
lua << EOF
require"psql".setup { }
EOF

""
"" aeriel
""
lua << EOF
require"aerial".setup { }
EOF
