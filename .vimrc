""
"" Flamenless Vim Configuration
""
"" A .vimrc that uses VimPlug and borrows heavily from Janus.
""
"" A "flamen" is an ancient Roman priest. An absence of any flamen is meant to
"" suggest the absence of any formal ties to the official maintainers of Janus.
""

""
"" Basic Setup
""
let mapleader = "\<Space>"       " Bring Space to your Leader
let maplocalleader = "\<Space>"  " And support your local providers
set number                       " Show line numbers
set ruler                        " Show line and column number
syntax enable                    " Turn on syntax highlighting allowing local overrides
set wildmode=longest,list        " bash style tab completion

""
"" Identify Syntaxes
""
if has("autocmd")
  au BufRead,BufNewFile *.har set syntax=json filetype=json

  au BufRead,BufNewFile *.{md,markdown,mdown,mkd,mkdn} setf markdown

  au BufRead,BufNewFile */nginx/* set ft=nginx

  au BufRead,BufNewFile *.sshconfig set ft=sshconfig

  au BufRead,BufNewFile dockerfile set ft=dockerfile

  au BufRead,BufNewFile *.tsx set ft=typescript

  au BufRead,BufNewFile Taskfile.* setf yaml

  au BufRead,BufNewFile .gitconfig* setf gitconfig
endif

""
"" Spacing (Janus)
""
set nowrap                      " don't wrap lines
set tabstop=2                   " Set tabs to equal 2 spaces
set shiftwidth=2                " an autoindent (with <<) is two spaces
set softtabstop=2
set expandtab                   " use spaces, not tabs
set list                        " Show invisible characters
set backspace=indent,eol,start  " backspace through everything in insert mode
set nojoinspaces                " Insert only one space when joining lines that contain
                                " sentence-terminating punctuation like `.`.
" List chars
set listchars=""                  " Reset the listchars
set listchars=tab:\ \             " a tab should display as "  ", trailing whitespace as "."
set listchars+=trail:.            " show trailing spaces as dots
set listchars+=extends:>          " The character to show in the last column when wrap is
                                  " off and the line continues beyond the right of the screen
set listchars+=precedes:<         " The character to show in the last column when wrap is
                                  " off and the line continues beyond the left of the screen

" https://stackoverflow.com/a/1675752/199644
" Set up characters for displaying whitespace.
" Turn on with :set list. Off with :set nolist
" TODO Resolve conflict with Janus-influenced configuration
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<,space:.,nbsp:_
set nolist

set nofoldenable                " Don't fold code by default

" Make vim break lines without breaking words
" Based on http://vimcasts.org/episodes/soft-wrapping-text/
command! -nargs=* Wrap setlocal wrap linebreak nolist
" And https://michaelsoolee.com/vim-toggle-word-wrap/
command! -nargs=* WrapToggle setlocal wrap! linebreak! nolist!
nmap <leader>vw :WrapToggle<CR>

" Disable autoindentation (http://bit.ly/IQKws0)
command! DisableAutoIndent setl noai nocin nosi inde=

""
"" Install plugins
"" See https://vimawesome.com for more
""
"" TODO https://github.com/junegunn/vim-plug/wiki/faq#managing-dependencies
""
call plug#begin('~/.vim/plugged')
  Plug 'airblade/vim-gitgutter'
  Plug 'alunny/pegjs-vim'
  Plug 'ap/vim-css-color'
  Plug 'arthurxavierx/vim-caser'
  Plug 'Asheq/close-buffers.vim'
  Plug 'bakpakin/fennel.vim'
  Plug 'c9s/tinyurl.vim'
  Plug 'chr4/nginx.vim'
  Plug 'chrisbra/csv.vim'
  Plug 'tpope/vim-classpath'
  Plug 'cocopon/iceberg.vim'
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
        \ | Plug 'junegunn/fzf.vim'
        \ | Plug 'pbogut/fzf-mru.vim'
        \ | Plug 'stsewd/fzf-checkout.vim'
  Plug 'junegunn/vim-easy-align'
  Plug 'jxnblk/vim-mdx-js'
  Plug 'editorconfig/editorconfig-vim'
  Plug 'ervandew/supertab'
  Plug 'freitass/todo.txt-vim'
  Plug 'kana/vim-textobj-user'
        \ | Plug 'glts/vim-textobj-comment'
  Plug 'glench/vim-jinja2-syntax'
  Plug 'guns/vim-sexp'
        \ | Plug 'tpope/vim-sexp-mappings-for-regular-people'
  Plug 'harrisoncramer/psql'
  Plug 'hashivim/vim-terraform'
  Plug 'lervag/wiki.vim'
  Plug 'majutsushi/tagbar'
  Plug 'martinda/Jenkinsfile-vim-syntax'
  Plug 'mattn/webapi-vim'
        \ | Plug 'mattn/gist-vim'
  Plug 'maxmellon/vim-jsx-pretty'
  Plug 'michaeljsmith/vim-indent-object'
  Plug 'mmalecki/vim-node.js'
  Plug 'mracos/mermaid.vim'
  Plug 'mzlogin/vim-markdown-toc'
  Plug 'othree/html5.vim'
  Plug 'pangloss/vim-javascript'
  Plug 'godlygeek/tabular'
  Plug 'plasticboy/vim-markdown'
  Plug 'Raimondi/delimitMate'
  Plug 'rgarver/Kwbd.vim' " Keep window on buffer delete
  Plug 'rhysd/devdocs.vim'
  Plug 'scrooloose/nerdcommenter'
  Plug 'shmup/vim-sql-syntax'
  Plug 'sjl/gundo.vim'
  Plug 'telamon/vim-color-github'
  Plug 'mg979/vim-visual-multi'
  Plug 'thinca/vim-visualstar'
  Plug 'tomtom/tlib_vim'
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-dispatch'
  Plug 'tpope/vim-eunuch'
  Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
  Plug 'tpope/vim-fugitive'
        \ | Plug 'https://codeberg.org/thisgeek/lam.vim.git'
  Plug 'tpope/vim-projectionist'
  Plug 'tpope/vim-ragtag'
  Plug 'tpope/vim-repeat'
  Plug 'tpope/vim-rhubarb'
  Plug 'tpope/vim-surround'
  Plug 'tpope/vim-unimpaired'
  Plug 'tpope/vim-vinegar'
  Plug 'troydm/zoomwintab.vim'
  Plug 'vim-airline/vim-airline'
  Plug 'vito-c/jq.vim'
  Plug 'voldikss/vim-search-me'
  Plug 'will133/vim-dirdiff'
  if has('nvim')
    Plug 'folke/zen-mode.nvim'
    Plug 'folke/which-key.nvim'
    Plug 'ldelossa/litee.nvim'
          \ | Plug 'ldelossa/gh.nvim'
    Plug 'neovim/nvim-lspconfig'
    Plug 'radenling/vim-dispatch-neovim'
    Plug 'sotte/presenting.nvim'
    Plug 'stevearc/aerial.nvim'
    Plug 'udayvir-singh/tangerine.nvim'
    Plug 'udayvir-singh/hibiscus.nvim'
  endif
call plug#end()

""
"" Color
""
set t_Co=256 " Take advantage of 256 color terminal
set background=dark " Work in the dark
set termguicolors " Enable 24-bit color
color iceberg

"""
""" Airline
"""
let g:airline#extensions#zoomwintab#enabled = 1
let g:airline_powerline_fonts = 1

""
"" Searching (Janus)
""
set hlsearch    " highlight matches
set incsearch   " incremental searching
set ignorecase  " searches are case insensitive...
set smartcase   " ... unless they contain at least one capital letter

""
"" Wild settings (Janus)
""
" Disable output and VCS files
set wildignore+=*.o,*.out,*.obj,.git,*.rbc,*.rbo,*.class,.svn,*.gem
" Disable archive files
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz
" Disable temp and backup files
set wildignore+=*.swp,*~,._*
" Disable mac files
set wildignore+=.DS_Store

""
"" Backup and swap files (Janus)
""
set backupdir^=~/.vim/_backup//    " where to put backup files.
set directory^=~/.vim/_temp//      " where to put swap files.

""
"" DelimitMate
""
let delimitMate_excluded_ft = "clj,cljs,edn"

" Disabled while experimenting with surround, e.g.
" https://github.com/tpope/vim-surround/issues/15#issuecomment-261358283
" or https://github.com/tpope/vim-surround/pull/226
" augroup plugin-delimitMate
"   autocmd!
"   " Configure better trip-tic behavior for markdown
"   " Credit: https://github.com/Raimondi/delimitMate/issues/238#issuecomment-303674751
"   au FileType markdown,gitcommit let b:delimitMate_expand_cr = 2
"   au FileType markdown,gitcommit let b:delimitMate_expand_inside_quotes = 1
"   au FileType markdown,gitcommit let b:delimitMate_expand_space = 0
"   au FileType markdown,gitcommit let b:delimitMate_nesting_quotes = ['`']
" augroup END

""
"" Markdown
""
augroup plugin-markdown
  autocmd!

  " Setup default wrapping behavior for markdown
  au FileType markdown setlocal wrap linebreak textwidth=80 nolist
  au FileType markdown setlocal conceallevel=2

  " Mappings for dec or inc headers
  au FileType markdown nmap <buffer> <leader>] :HeaderIncrease<CR>
  au FileType markdown nmap <buffer> <leader>[ :HeaderDecrease<CR>
  au FileType markdown vmap <buffer> <leader>] :HeaderIncrease<CR>
  au FileType markdown vmap <buffer> <leader>[ :HeaderDecrease<CR>
augroup END

" Support strikethrough rendering in markdown
" Adapted from https://stackoverflow.com/a/69682894
let g:vim_markdown_strikethrough = 1
if &term =~ 'xterm\|kitty\|alacritty\|tmux'
    let &t_Ts = "\e[9m"   " Strikethrough
    let &t_Te = "\e[29m"
endif

" Use a stronger contrast for markdown blockquotes
hi! def link mkdBlockquote Constant

""
"" Markdown TOC
""
" TODO
" - Config per project
" - Rplace with wiki.vim functions
let g:vmt_fence_text = "toc"
let g:vmt_fence_closing_text = "tocstop"
let g:vmt_fence_hidden_markdown_style = "GFM"

""
"" Git Commits
""
" Check spelling https://stackoverflow.com/a/57690465/199644
autocmd FileType gitcommit setlocal spell
" https://vi.stackexchange.com/questions/3990/ignore-urls-and-email-addresses-in-spell-file
autocmd FileType gitcommit :syn match UrlNoSpell "\w\+:\/\/[^[:space:]]\+" contains=@NoSpell
" Uses regex from https://vi.stackexchange.com/a/22161/22505
autocmd FileType gitcommit :syn match MDCodeSpanNoSpell "\v([`]).{-}[^\\]\1" contains=@NoSpell

""
"" Pull Requests
""
" Check spelling https://stackoverflow.com/a/57690465/199644
autocmd BufRead PULLREQ_EDITMSG setlocal spell
" Break lines https://vim.fandom.com/wiki/Word_wrap_without_line_breaks
autocmd BufRead PULLREQ_EDITMSG :Wrap

""
"" Fugitive
""
" See lam.vim

""
"" GitGutter
""
let g:gitgutter_preview_win_floating = 0
" https://github.com/airblade/vim-gitgutter#when-signs-take-a-few-seconds-to-appear
set updatetime=300

" Git add all changes in the current file
nmap <leader>ha :Git add %<CR>

""
"" FZF
""

" Explicitly include fzf in the runtime path, as recommended by brew install.
" Disabled as possibly unecessary so long as vim-plug handles `fzf#install()`.
" set rtp+=/usr/local/opt/fzf

" What was once Ctrl-P is now fzf.
map <c-p> :GFiles<CR>
" TODO Restore 'smart' finding functionality with fzf-mru or similar.

" TODO Support listing files relative to the path from the current buffer.
" Experimental.
command! -complete=dir -nargs=* Groot
      \ call fzf#run(fzf#wrap({
      \   'source': 'git rev-parse --show-toplevel | xargs git ls-files',
      \   'dir': <q-args>
      \ }))

" Mapping for fzf-checkout
nmap <Leader>zr :GBranch<CR>

" Mapping for silver searcher
nmap <silent> <Leader>za :Ag<CR>
" Run fzf.vim's Ag command with the word or WORD under cursor
" Warning: experimental
" From https://github.com/junegunn/fzf.vim/issues/50#issuecomment-161676378
nmap <silent> <Leader>zw :Ag <C-R><C-W><CR>
nmap <silent> <Leader>zW :Ag <C-R><C-A><CR>

nmap <Leader>zb :Buffers<CR>

nmap <Leader>zf :FZF

" Fallback: Fuzzy-finder depends on the environment, which breaks the
" portability of this configuration. The following offers an alternative if
" fuzzy-finder is not available.
"
" Caveats: Tab completion via path will have terrible performance in
" sufficiently large codebases. If troubleshooting, remember that any behavior
" is likely affected by all other wild settings.
"
" Experimental. If insufficient, consider installing ctrlp.vim.
set path+=**
set wildmenu

""
"" Javascript
""
let g:javascript_plugin_jsdoc = 1    " Enable syntax highlighting for JSDocs
let g:vim_jsx_pretty_highlight_close_tag = 1 " Highlight the close tag separately from the open tag

" Map K to query devdocs for javascript files as per the devdocs README
augroup plugin-devdocs
  autocmd!
  autocmd FileType javascript,html nmap <buffer>K <Plug>(devdocs-under-cursor)
augroup END

" Add convenient command for formatting json
" Requires: https://stedolan.github.io/jq/
function! Jsonify()
  set filetype=json
  if executable("jq")
    %!jq '.'
  else
    echom "jq not installed"
  endif
endfunction
command! Jsonify :call Jsonify()


""
"" Clojure
""
" Largely borrowed from https://soundcloud.com/user-959992602/s2-e8-vim-setup-with-dominic-monroe

if has("autocmd")
  autocmd FileType clojure setlocal textwidth=80
  autocmd FileType clojure setlocal colorcolumn=+1
  " Support block comment continuation
  " References https://vimhelp.org/change.txt.html#fo-table
  autocmd FileType clojure setlocal formatoptions+=o

  autocmd FileType clojure let b:delimitMate_quotes = "\""
endif

" Grey out discarded forms
" See https://github.com/clojure-vim/clojure.vim?tab=readme-ov-file#gclojure_discard_macro
let g:clojure_discard_macro = 1

" From sexp doc:
" Clojure:
"   Use the FireplacePrint operator from fireplace.vim [2] to evaluate
"   the current top-level compound form, compound form, or element
"   without moving the cursor.
nmap <Leader>F <Plug>FireplacePrint<Plug>(sexp_outer_top_list)``
nmap <Leader>f <Plug>FireplacePrint<Plug>(sexp_outer_list)``
nmap <Leader>e <Plug>FireplacePrint<Plug>(sexp_inner_element)``

let g:sexp_enable_insert_mode_mappings = 0

" Disable word maps from vim-sexp-mappings-for-regular-people
let g:sexp_no_word_maps = 1

" Similar to vim-salve, if directory contains dev.cljs.edn, presume figwheel
" provides the cljs repl.
"
" Depends on ~/.clojure/deps setting :repl/pig alias
let g:projectionist_heuristics = {
      \   "deps.edn&dev.cljs.edn": {
      \     "*": {
      \       "console": "clojure -M:repl/pig",
      \       "cljsRepl": "(do (require 'figwheel.main.api) (figwheel.main.api/start \"dev\"))"
      \     }
      \   }
      \ }

""
"" Search Me
""
" https://github.com/voldikss/vim-browser-search#keymappings
nmap <silent> <Leader>s <Plug>SearchNormal
vmap <silent> <Leader>s <Plug>SearchVisual
let g:browser_search_default_engine = 'duckduckgo'

""
"" Projectionist
""

" Mapppings for alternate file navigation
nmap <Leader>aa :A<CR>
nmap <Leader>as :AS<CR>
nmap <Leader>av :AV<CR>
nmap <Leader>at :AT<CR>

""
"" Easy Align
""
" Start interactive EasyAlign in visual mode (e.g. vipgz)
xmap gz <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gzip)
nmap gz <Plug>(EasyAlign)


""
"" Wiki.vim
""
let g:wiki_root = fnamemodify('~/vimwiki', ':p:h')

" TODO
" - Expand path from % to account for outside pwd
" - Compare `Dispatch!` to `call jobstart()`
" - Relative path bug: % is fullpath unless pwd is wiki_root
function! CommitChangeInWiki()
  let s:auto_commit_script = 'git -C ' . g:wiki_root . ' add "%"'
  let s:auto_commit_script .= ' && git -C ' . g:wiki_root . ' commit -m "Auto commit of %:t." "%"'
  let s:auto_commit_script .= ' && git -C ' . g:wiki_root . ' push origin'
  execute 'Dispatch! ' . s:auto_commit_script
endfunction

augroup plugin-wikivim
  autocmd!
  execute 'au! BufWritePost '. g:wiki_root . '/** call CommitChangeInWiki()'
augroup END

" Based on https://github.com/lervag/wiki.vim/issues/229#issuecomment-1164611576
function! AddWikiTmpl(tmpl_dict)
  let g:wiki_templates = get(g:, 'wiki_templates', [])
  if filereadable(a:tmpl_dict.source_filename)
    let g:wiki_templates += [ a:tmpl_dict ]
  else
    echoerr a:tmpl_dict.source_filename . ' is not readable'
  endif
endfunction

" Use local template for new journal and vs entries
let s:wiki_journal_template = g:wiki_root . '/.templates/journal.md'
call AddWikiTmpl(
      \ {'match_re': '^\d\d\d\d-\d\d-\d\d$',
      \ 'source_filename': s:wiki_journal_template})
call AddWikiTmpl(
      \ {'match_re': ' vs ',
      \  'source_filename': g:wiki_root . '/.templates/vs.md'})

" TODO:
" - Override weekly and monthly templates
" - Note inside of wiki.vim/autoload/template.vim
"   - wiki#template#init
"   - wiki#template#weekly_summary
" - wiki.vim/test/test-journal/test-dates.vim
"
" Example:
" call AddWikiTmpl(
"       \ {'match_re': '^\d\d\d\d_w\d\d$'
"       \  'source_filename': g:wiki_root . '/.templates/weekly-journal.md'})

" Find wiki tags toward the end of the file
let g:wiki_tag_scan_num_lines = -5

" Filter wiki navigation lists with fzf
let g:wiki_select_method = {
      \ 'pages': function('wiki#fzf#pages'),
      \ 'tags': function('wiki#fzf#tags'),
      \ 'toc': function('wiki#fzf#toc'),
      \}

" Add custom mappings for temporal journal navigation
let g:wiki_mappings_local_journal = {
      \ '<plug>(wiki-journal-prev)' : '<<',
      \ '<plug>(wiki-journal-next)' : '>>',
      \}

" Alias
nmap <Leader>wzp :WikiPages<CR>
nmap <Leader>wzt :WikiTags<CR>
nmap <Leader>wzc :WikiToc<CR>

" Always change local dir to wiki root when opening wiki journal
nmap <Leader>w<Leader>w :execute 'lcd' . g:wiki_root . '\| WikiJournal'<CR>


""
"" Vim tabs
""
nmap <Leader>bb :tabnew<CR>
nmap <Leader>bq :tabclose<CR>


""
"" Uncategorized
""

" Y should logically yank to end of line
map Y y$

" Copy selection to clipboard
vmap <leader>y "*y

" Disable Ex mode shortcut
map Q <Nop>

" E = Move to End of previous word
nmap E ge

" Clear the last used search pattern
command! ClearSearch nohlsearch
nmap <silent> <leader>hc :ClearSearch<CR>

" Shortcut common directory exploration commands
nmap <leader>ev :Vex<CR>
nmap <leader>es :Sex<CR>
nmap <leader>ee :e .<CR>

" Open with
nmap <leader>oo :!open "%"<CR>

" Toggle spelling 
nmap <leader>vs :setlocal spell!<CR>

" Safely permit netrw to remove non-empty local directories
" Depends on https://hasseg.org/trash/ or
" https://github.com/andreafrancia/trash-cli
let g:netrw_localrmdir='trash'

" From https://vi.stackexchange.com/a/16083/22505
" Delete all Git conflict markers
" Creates the command :GremoveConflictMarkers
function! RemoveConflictMarkers() range
  echom a:firstline.'-'.a:lastline
  " TODO Does the regex pattern account for names in the markers? Probably
  " deletes the whole line for any partial match.
  execute a:firstline.','.a:lastline . ' g/^<\{7}\|^|\{7}\|^=\{7}\|^>\{7}/d'
endfunction
"-range=% default is whole file
command! -range=% GremoveConflictMarkers <line1>,<line2>call RemoveConflictMarkers()

command! ConfReload source $MYVIMRC

command! ToggleLineNumbers set number!
nmap <leader>vn :ToggleLineNumbers<CR>

" Use contiguously vertical characters for gitgutter
" Relies on parts of Unicode's "Block Elements Block"
" Reference: Block Elements https://unicode.org/charts/nameslist/n_2580.html
" Reference: Box Drawing Block https://unicode.org/charts/nameslist/c_2500.html
let g:gitgutter_sign_added = '▕'
let g:gitgutter_sign_modified= '┋'
let g:gitgutter_sign_removed = '▏'
" Defaults
" let g:gitgutter_sign_removed_first_line = '‾'
" let g:gitgutter_sign_removed_above_and_below = '_¯'
" let g:gitgutter_sign_modified_removed  = '~_'

" Diff
" https://unix.stackexchange.com/questions/1386/comparing-two-files-in-vim
command! Diffit windo diffthis

" Syntax highlighting under cursor
" From https://stackoverflow.com/questions/9464844/how-to-get-group-name-of-highlighting-under-cursor-in-vim
function! SynStack()
  if !exists("*synstack")
    return
  endif
  return map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunction

function! SynGroup()
  let l:s = synID(line('.'), col('.'), 1)
  return [synIDattr(l:s, 'name'), synIDattr(synIDtrans(l:s), 'name')]
endfunction
