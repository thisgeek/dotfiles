# Private

# Load private env
if [ -f ~/.env_private ]; then \
  set -a; . ~/.env_private; set a+;
fi

# PATH

export PATH=$HOMEBREW_PREFIX/bin:$HOMEBREW_PREFIX/sbin:$PATH # Prefer homebrew installed software to the system's
export PATH=$HOME/.bin:$PATH                     # Add home scripts
export PATH=./.bin:./bin:$PATH                   # Prefer local project scripts to all paths

# Add homebrew binaries to path
export PATH="$HOMEBREW_PREFIX/opt/make/libexec/gnubin:$PATH"      # make
export PATH="$HOMEBREW_PREFIX/opt/sqlite/bin:$PATH"               # sqlite
export PATH="$HOMEBREW_PREFIX/opt/libpq/bin:$PATH"                # psql
export PATH="$HOMEBREW_PREFIX/opt/python@3.10/libexec/bin:$PATH"  # python (unversioned)

# Application binaries

# Add Gimp binaries (interactive batch mode does not work as expected)
# export PATH=$PATH:/Applications/Gimp.app/Contents/Resources/bin

# Preferred editor
set -o vi
export EDITOR=nvim

# Emulate oh-my-zsh edit command line in editor
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd 'vv' edit-command-line

# Go
export GO111MODULE=on
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

# Aliases

alias nv=nvim

# Initialize chrome from shell (in order to take advantage of options)
# Alt: https://stackoverflow.com/questions/26388405/chrome-disable-ssl-checking-for-sites#comment96711735_46702756
# Alt: Cargo cult the configurations used by selenium web driver. See link above.
alias chrome="open -a Google\ Chrome --args"
alias proxy-chrome="chromium --ignore-certificate-errors --ignore-urlfetcher-cert-requests --proxy-server=http://localhost:9898 --proxy-bypass-list='<-loopback>'"

# "Renew" lease on dynamic hosts config. Sometimes useful for healing
# misbehavior in atypical network clients. May need to be run with root
# permissions. Resembles recommendations in
# https://apple.stackexchange.com/a/17429/11085
alias renew-dhcp-lease="ipconfig set en0 DHCP"

# From https://superuser.com/q/702156
function rename_tab {
  echo -ne "\033]0;"$@"\007"
}

function git-change-tree {
  cd $(git worktree list | fzf | cut -f1 -d' ')
}

# Other Settings

# Simplify prompt
export PS1='\W \$ '

PROMPT="%1~ > "
if [[ -n "$TMUX" ]]; then
  PROMPT="%1~ » "
fi

# Enable extended globs
# shopt -s extglob

# Color

export GREP_OPTIONS='--color=auto'

# Generically colourize logfiles and command output with gh:garabik/grc
[[ -s "$HOMEBREW_PREFIX/etc/grc.zsh" ]] && source $HOMEBREW_PREFIX/etc/grc.zsh
# Prevent grc from interfering with interactive repls made by make
# Disabled to quiet the following errors:
# ~/.zshrc:unset:84: no such hash table element: make
# ~/.zshrc:unset:85: no such hash table element: gmake
# unset -f make
# unset -f gmake

# Load homebrew's zsh completion plugins
fpath=($HOMEBREW_PREFIX/share/zsh-completions $fpath)
autoload -Uz compinit && compinit

# Bind Ctrl-R to reverse history search
# Ref: https://unix.stackexchange.com/a/30169/17796
# Remember that vi edit mode defaults to / or ? while in input mode
# But Ctrl-R is easier to type quickly than stretching for the esc key
bindkey -v
bindkey '^R' history-incremental-pattern-search-backward

export HISTFILESIZE=100000
export HISTSIZE=10000
export SAVEHIST=$HISTSIZE

export HISTORY_IGNORE="clear|exit|fg|jobs|ls|nv|git status|git graph"

# Support pgp encryption over teletype
export GPG_TTY=$(tty)

# Include fuzzy finder completion
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export MANPATH="$MANPATH:$DOCKER_MANPATH"

# Use neovim for man pages
export MANPAGER="$HOMEBREW_PREFIX/bin/nvim -m -c 'Man!' -o -"
function vman() {
  nvim -mR -c "Man $1" -c "only"
}

# Include trash binaries
export PATH="$HOMEBREW_PREFIX/opt/trash/bin:$PATH"
